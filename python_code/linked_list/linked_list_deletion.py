class Node:
    def __init__(self,data):
        self.data = data
        self.next = None

    
class LinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self.count = 0

    def insert(self,data):
        new_node = Node(data)
        if(self.head == None):
            self.head = new_node
            self.next = None
        else:
            cur_node = self.head
            while(cur_node.next):
                cur_node = cur_node.next
            print("inserting node")
            cur_node.next = new_node
        self.count += 1 

    def deleteFirst(self):
        if(self.head == None):
            print("list is empty, cannot delete element")
            return

        temp = self.head
        self.head = self.head.next
        temp = None
        self.count -= 1

    def deleteLast(self):
        if(self.head == None):
            print("list is empty, cannot delete element")
            return
        cur_node = self.head
        prev_node = None
        while(cur_node.next):
            prev_node = cur_node
            cur_node = cur_node.next
        prev_node.next = None
        self.tail = prev_node
        
        cur_node = None
        self.count -= 1
        

    def deleteNode(self,key):
        i = 1
        cur_node = self.head
        prev_node = None

        if cur_node == None:
            print("empty list, cannot delete anything")
            return

        elif self.head.data == key:
            self.deleteFirst()

        
        else:   
            while(cur_node.data != key):
                prev_node = cur_node
                cur_node = cur_node.next
                i = i + 1


            prev_node.next = cur_node.next
            cur_node = None
            self.tail = prev_node
            print("deleting node at position ",i)
            self.count -= 1

    def printList(self):
        next_node = self.head
        while(next_node):
            print(next_node.data," --> ",end="" )
            next_node = next_node.next
        print("None")


llist = LinkedList()
llist.insert(1)
llist.insert(2)
llist.insert(3)
llist.insert(4)
llist.insert(5)
llist.insert(6)
llist.printList()
llist.deleteLast()
llist.printList()

