from linked_list_insertion import Node
from linked_list_insertion import LinkedList

def reverse(llist):
    
    if(not llist.head):
        print("list is empty")
        return

    slow_ptr = llist.head
    fast_ptr = llist.head

    #find the middle element
    while(fast_ptr and fast_ptr.next):
        fast_ptr = fast_ptr.next.next


    cur_node = llist.head
    prev_node = None

    while(cur_node):
        next_node = cur_node.next
        cur_node.next = prev_node
        prev_node = cur_node
        cur_node = next_node
        

    