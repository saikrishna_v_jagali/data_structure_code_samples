from linked_list_insertion import Node
from linked_list_insertion import LinkedList

llist = LinkedList()
llist.insertFront(9)
llist.insertFront(8)
llist.insertFront(7)
llist.insertFront(6)
llist.insertFront(5)
llist.insertFront(4)
llist.insertFront(3)
llist.insertFront(2)
llist.insertFront(1)

llist.printList()



def exchange_nodes(llist,key1,key2):
    '''four cases to be consider
    * if 
    '''

    cur_node = llist.head
    prev1_node = None
    next1_node = None
    key1_node = None
    node1_found = False
    head1_node = None

    prev2_node = None
    next2_node = None
    key2_node  = None
    node2_found = False
    head2_node = None
    
    if(key1 == llist.head.data):
        head1_node = llist.head
    elif(key2 == llist.head.data):
        head2_node = llist.head

    while(cur_node): 
        if(cur_node.data == key1):
            key1_node  = cur_node
            next1_node = cur_node.next 
            node1_found = True

        elif(cur_node.data == key2):
            key2_node = cur_node
            next2_node = cur_node.next
            node2_found = True

        if(not node1_found): 
            prev1_node = cur_node 
        if(not node2_found):
            prev2_node = cur_node

        cur_node = cur_node.next


    # print("prev1",prev1_node.data)
    print("prev2",prev2_node.data)

    #if key1 is head and key2 is also found
    if(head1_node and node2_found):
        key2_node.next = llist.head.next
        llist.head = key2_node
        prev2_node.next = head1_node
        head1_node.next = next2_node
        

    elif(head2_node and node1_found):
        key1_node.next = llist.head.next
        llist.head = key1_node
        prev1_node.next = head2_node
        head2_node.next = next1_node
        

    elif(node1_found and node2_found):
        print("both nodes are found")
        prev1_node.next = key2_node
        key2_node.next = next1_node

        prev2_node.next = key1_node
        key1_node.next = next2_node

    else:
        print("Some of entered nodes are not in the list")

    

#exchange two nodes
exchange_nodes(llist,1,9)
llist.printList()
