import time
class Node:
  
    #function to initialise the node object
    def __init__(self,data):
        self.data = data 
        self.next = None   


class LinkedList:

    #function to intiailise the head
    def __init__(self):
        self.head = None
        self.tail = None
        self.count = 0

    #================Insertion=============================
    #Insert at front of linkedList
    def insertFront(self,data):
        new_node = Node(data)
        if self.head == None:
            print("inserting **HEAD** [",data, "] from the front")           
            self.head = new_node  
            self.tail = new_node                      
        else:
            print("inserting node [",data ," ] from the front")
            new_node.next = self.head
            self.head = new_node    

        self.count = self.count + 1

    #Insert At Last of LinkedList 
    def insertLast(self,data):
        if self.head == None:
            print("Inserting **HEAD** [", data,"] from the back")
            new_node = Node(data)
            self.head = new_node
            self.tail = new_node            
        else:
            print("Inserting node [",data, "] from the back")
            new_node  = Node(data)
            # next_node = self.head
            # animate_function(self,next_node)
            
            # while(next_node.next):
            #     next_node = next_node.next
            #     animate_function(self,next_node)

            # next_node.next = new_node
            self.tail.next = new_node
            self.tail = new_node
            # animate_function(self,next_node)
            # animate_function(self,next_node.next)
        self.count = self.count + 1

    #Insert at a given position
    def insertAtPosition(self,data,insert_pos):
        pos = 1
        cur_node = self.head
        new_node = Node(data)
    
        if insert_pos == 1:
            self.insertFront(data)

        elif(pos == self.count):
            self.insertLast(data)

        elif pos > self.count:
            print("position is higher than greater than list length")
        
        else:
            while(pos < insert_pos-1):
                pos = pos + 1
                cur_node = cur_node.next
                animate_function(self,cur_node)
            print("inserting node at position ",pos)
            new_node.next = cur_node.next
            cur_node.next = new_node
            self.count = self.count + 1


    def insertFrontFromArray(self,arr):
        for i in arr:
            self.insertFront(i)

    def insertRearFromArray(self,arr):
        for i in arr:
            self.insertLast(i)

    ##====================deletion=====================
    def deleteFirst(self):
        if(self.head == None):
            print("list is empty, cannot delete element")
            return

        temp = self.head
        self.head = self.head.next
        temp = None
        self.count -= 1

    #Before deleting last node,get a ref to previous node and make it a tail node
    def deleteLast(self):
        if(self.head == None):
            print("list is empty, cannot delete element")
            return

        cur_node = self.head
        prev_node = None
        while(cur_node.next):
            prev_node = cur_node
            cur_node = cur_node.next
        prev_node.next = None
        self.tail = prev_node
        
        cur_node = None
        self.count -= 1
        

    def deleteNode(self,key):
        i = 1
        cur_node = self.head
        prev_node = None

        if cur_node == None:
            print("empty list, cannot delete anything")
            return

        elif self.head.data == key:
            self.deleteFirst()
      
        else:   
            while(cur_node.data != key):
                prev_node = cur_node
                cur_node = cur_node.next
                i = i + 1

            prev_node.next = cur_node.next
            cur_node = None
            self.tail = prev_node
            print("deleting node at position ",i)
            self.count -= 1



    #print LinkedList               
    def printList(self):
        next_node = self.head
        while(next_node):
            print(next_node.data," --> ",end="" )
            next_node = next_node.next
        print("None")



def animate_function(llist,focus_node):
    next_node = llist.head
    time.sleep(2)
    while(next_node):
        if next_node.next == focus_node.next:
            print(" **[[",next_node.data,"]]** -->", end="")
        else:
            print(next_node.data," --> ",end="" )
        next_node = next_node.next
    print("NoneOO")
    print("\n")


if __name__=="__main__":
    llist =  LinkedList()

    print("heelo")

    llist.insertFrontFromArray([1,2,3,4,5,6])
    llist.insertRearFromArray([1,2,3,4,5,6])
    llist.printList()