class Node:
    def __init__(self,data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self,data):
        new_node = Node(data)
        if(not self.head):
            self.head = new_node
            return
        
        cur_node = self.head
        while(cur_node.next):
            cur_node = cur_node.next
        cur_node.next = new_node

    def search(self,key):
        pos = 1
        if(key == None):
            print("no key to search")
            return

        if(not self.head):
            print("list is empty")
            return

        cur_node = self.head
        while(cur_node):
            if(cur_node.data == key):
                print("list has the key at position ",pos)
                return
            pos = pos + 1
            cur_node = cur_node.next
        
        print("linked list doesnot contain element")


    def printList(self):
        next_node = self.head
        while(next_node):
            print(next_node.data," --> ",end="" )
            next_node = next_node.next
        print("None")

    
            
            
llist = LinkedList()
llist.insert(1)
llist.insert(2)
llist.insert(3)
llist.insert(4)
llist.insert(5)

cur = llist.head
loop_ptr = 2
while(cur.next):
    if(cur.data == loop_ptr):
       loop_start_node = cur 
    cur = cur.next


cur.next = loop_start_node
# llist.printList()

# identify the loop
if not llist.head:
    print("empty list")

slow_ptr = llist.head
fast_ptr = llist.head.next

while(slow_ptr != fast_ptr):
    slow_ptr = slow_ptr.next
    fast_ptr = fast_ptr.next.next

if slow_ptr != fast_ptr:
    print("Llist doesnot contain loop")
    # return

loop_length = 1
loop_head_node = slow_ptr
cur_node1 = slow_ptr
print("slow ptr data =",slow_ptr.data)
print("fast ptr data =",fast_ptr.data)
print("loop_head node data =",loop_head_node.data)
print("cur node data =",cur_node1.data)
while(cur_node1.next != slow_ptr):
    print(cur_node1.next.data, " != ", loop_head_node.data)
    loop_length = loop_length + 1
    cur_node1 = cur_node1.next
    print("cur_node=",cur_node1.data)

print("loop length = ",loop_length)



# llist.printList()