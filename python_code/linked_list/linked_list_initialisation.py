class Node:
    
    #function to initialise the node object
    def __init__(self,data):
        self.data = data  #initialise the node with data
        self.next = None  #make the next point to null

#Linked List  class contains the node object
class LinkedList:

    #function to intiailise the head
    def __init__(self):
        self.head = None
        self.tail = None
        self.count = None

if __name__=="main":
    #initialise the linkedLIst
    llist =  LinkedList()
    llist.head = Node(1)
    llist.head.next = Node(2)
    llist.head.next.next = Node(3)

    print("heelo")
    print(llist.head.data)
    print(llist.head.next.data)