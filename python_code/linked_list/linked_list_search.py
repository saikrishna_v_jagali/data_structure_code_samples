class Node:
    def __init__(self,data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self,data):
        new_node = Node(data)
        if(not self.head):
            self.head = new_node
            return
        
        cur_node = self.head
        while(cur_node.next):
            cur_node = cur_node.next
        cur_node.next = new_node

    def search(self,key):
        pos = 1
        if(key == None):
            print("no key to search")
            return

        if(not self.head):
            print("list is empty")
            return

        cur_node = self.head
        while(cur_node):
            if(cur_node.data == key):
                print("list has the key at position ",pos)
                return
            pos = pos + 1
            cur_node = cur_node.next
        
        print("linked list doesnot contain element")
            
            
llist = LinkedList()
llist.insert(1)
llist.insert(2)
llist.insert(3)
llist.search(3)

