from linked_list_insertion import Node
from linked_list_insertion import LinkedList

def checkPalindrome():
    llist = LinkedList()
    llist.insertFront(1)
    llist.insertFront(2)
    llist.insertFront(3)
    llist.insertFront(4)
    llist.insertFront(5)
    llist.insertFront(5)
    llist.insertFront(4)
    llist.insertFront(3)
    llist.insertFront(2)
    llist.insertFront(1)

    llist.printList()
    # find the middle element and  process
    if(not llist.head):
        print("list empty")
        return

    slow_ptr = llist.head
    fast_ptr = llist.head
    prev_node = None
    
    
    while(fast_ptr and fast_ptr.next):
        prev_node =slow_ptr
        slow_ptr = slow_ptr.next
        fast_ptr = fast_ptr.next.next

    print("=============================")
    # print("middle element is ",slow_ptr.data)
    # print("fast_ptr is ",fast_ptr.data)


    #Note: If number of nodes are even, fast_ptr  will be pointing to NULL
    #      If number of nodes are odd it will point to non NULL node.
    if(fast_ptr != None):
        mid_node = slow_ptr
        slow_ptr.next = reverse(llist,mid_node.next)
        print("after reversing odd")
        llist.printList()
        compare(llist,first_ptr = llist.head,second_ptr=mid_node.next)


    else:
        mid_node = slow_ptr
        prev_node.next = reverse(llist,mid_node)
        print("after reversing even=====",slow_ptr.data)
        # print("after reversing even=====",slow_ptr.next.data)
        llist.printList()
        compare(llist,first_ptr = llist.head,second_ptr=mid_node.next)


def reverse(llist,start_ptr):        
    cur_node = start_ptr
    prev_node = None
    print("start_ptr=",start_ptr.data) 
    while(cur_node):
        print("cur_nd",cur_node.data)
        next_node = cur_node.next
        cur_node.next = prev_node
        prev_node = cur_node
        cur_node = next_node
       
    print("------") 
    llist.printList()
    return prev_node


def compare(llist,first_ptr,second_ptr):
    while(first_ptr and second_ptr):
        if(first_ptr.data != second_ptr.data):
            print("NOdes are not equal")
            return False
        first_ptr = first_ptr.next
        second_ptr = second_ptr.next
    print("nodes are equal")   
    return True

checkPalindrome()

