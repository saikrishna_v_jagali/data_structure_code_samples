import time
from linked_list_initialisation import LinkedList

class LinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self.count = 0

    def insertFront(self,data):
        new_node = Node(data)
        if self.head == None:
            print("inserting **HEAD** [",data, "] from the front")           
            self.head = new_node  
            self.tail = new_node          
             

        else:
            print("inserting node [",data ," ] from the front")
            new_node.next = self.head
            self.head = new_node
                  
        self.count = self.count + 1

    def insertLast(self,data):
        if self.head == None:
            print("Inserting **HEAD** [", data,"] from the back")
            new_node = Node(data)
            self.head = new_node
            self.tail = new_node
            


        else:
            print("Inserting node [",data, "] from the back")
            new_node  = Node(data)
            # next_node = self.head
            # animate_function(self,next_node)
            
            # while(next_node.next):
            #     next_node = next_node.next
            #     animate_function(self,next_node)

            # next_node.next = new_node
            self.tail.next = new_node
            self.tail = new_node
            # animate_function(self,next_node)
            # animate_function(self,next_node.next)

        self.count = self.count + 1

    def insertAtPosition(self,data,insert_pos):
        pos = 1
        cur_node = self.head
        new_node = Node(data)
    
        if insert_pos == 1:
            self.insertFront(data)

        elif(pos == self.count):
            self.insertLast(data)

        elif pos > self.count:
            print("position is higher than greater than list length")
        
        else:
            while(pos < insert_pos-1):
                pos = pos + 1
                cur_node = cur_node.next
                animate_function(self,cur_node)
            print("inserting node at position ",pos)
            new_node.next = cur_node.next
            cur_node.next = new_node
            self.count = self.count + 1

        
        

    def printList(self):
        next_node = self.head
        while(next_node):
            print(next_node.data," --> ",end="" )
            next_node = next_node.next
        print("None")



def animate_function(llist,focus_node):
    next_node = llist.head
    time.sleep(2)
    while(next_node):
        if next_node.next == focus_node.next:
            print(" **[[",next_node.data,"]]** -->", end="")
        else:
            print(next_node.data," --> ",end="" )
        next_node = next_node.next
    print("NoneOO")
    print("\n")

    
# llist = LinkedList()
# llist.insertFront(1)
# llist.insertFront(2)
# llist.insertFront(3)
# llist.printList()

# llist_rear =  LinkedList()
# llist_rear.insertLast(1)
# llist_rear.insertLast(2)
# llist_rear.insertLast(3)
# llist_rear.insertLast(4)
# llist_rear.insertLast(5)
# llist_rear.insertLast(6)
# llist_rear.printList()

# llist_insertAtPos = LinkedList()
# llist_insertAtPos.insertAtPosition(1,1)
# llist_insertAtPos.insertAtPosition(2,2)
# llist_insertAtPos.insertAtPosition(3,2)
# llist_insertAtPos.insertAtPosition(4,2)
# llist_insertAtPos.printList()



