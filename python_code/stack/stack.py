class Stack:
    def __init__(self,capacity):
        self.S = [None] * capacity
        self.size = 0
        self.capacity = capacity
        
    def push(self,item):
        if(self.size > self.capacity - 1):
            print("stack is full")
            return

        print("pushing " + str(item) + "to stack")
        self.S.append(item)
        self.size+= 1


    
    def pop(self):
        if(self.size == 0):
            print("stack is empty")
            return
        
        x = self.S.pop()
        self.size-= 1
        print("popped "+ str(x) +"from stack")
        return x

    def printStack(self):
        for i in (self.size,0):
            print(self.S[i])

if __name__=="__main__":
    stack = Stack(5)
    stack.push(1)
    stack.push(2)
    stack.push(3)
    stack.push(4)
    stack.push(5)
    stack.push(6)
    stack.pop() 
    stack.pop() 
    stack.pop() 
    stack.pop() 
    stack.pop() 
    stack.pop() 
    stack.pop() 
    stack.pop() 
            


