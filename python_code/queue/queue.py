class Queue:
    def __init__(self,n):
        self.Q = [None] * n
        self.capacity = n
        self.front = 0
        self.rear = n - 1
        self.size = 0 
        
    def isEmpty(self):
        return (self.size == 0 )

    def isFull(self):
        return self.size == self.capacity

    def enqueue(self,item):
        if(self.isFull()):
            print("queue is full")
            return 

        print("inserting ",item,' in the queue',self.rear,len(self.Q))
        self.Q[self.rear] = (item)
        self.rear= (self.rear + 1) % self.capacity 
        self.size+= 1

    def dequeue(self):
        if(self.isEmpty()):
            print("queue is empty")
            return
        
        print("Element ",self.Q[self.front] ,"is popped from queue")
        self.front= (self.front + 1) % self.capacity
        self.size-=  1

    def printQueue(self):
        print(self.front,"==",self.rear)
        front = self.front
        for i in range(0,self.size):
            print(self.Q[front], " -> ",end="")
            front=(front+1)% self.capacity

    def que_front(self):
        if(self.isEmpty()):
            print("empty queue")
            return 
        return self.Q[self.front]

    def que_rear(self):
        if(self.isEmpty()):
            print("empty queue")
            return 
        return self.Q[self.rear]



if __name__ == "__main__":
    print("creating queue of size 5")
    queue = Queue(5)
    queue.enqueue(1)
    queue.enqueue(2)
    queue.enqueue(3)
    queue.dequeue()
    queue.dequeue()
    queue.enqueue(4)
    queue.enqueue(5)
    queue.enqueue(6)
    queue.enqueue(7)
    queue.enqueue(8)
    queue.printQueue()
    print("\nfront ",queue.que_front())
    print("rear ",queue.que_rear())




        
