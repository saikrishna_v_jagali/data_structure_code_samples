#Implementing queue with costly enqueue operation
class Queue:

    def __init__(self):
        self.s1 = []
        self.s2 = []
 
    def enqueue(self,item):
        print("appending ",item)      
        while(len(self.s1) != 0):
            self.s2.append(self.s1.pop())
        print(self.s1)        
        self.s1.append(item)
        while(len(self.s2) != 0):
            self.s1.append(self.s2.pop())
        print(self.s1)

    def dequeue(self):
        if(len(self.s1) == 0):
            print("list is empty")
            return

        x = self.s1.pop()
        return x
        

        return x
            

if __name__ == 0"main": 
    print("executing")
    queue = Queue()
    queue.enqueue(1)
    queue.enqueue(2)
    queue.enqueue(3)
    queue.enqueue(4)
    print(queue.s1) 
    print("removing ",queue.dequeue())
    queue.enqueue(5)
    queue.enqueue(6)
    print("removing ",queue.dequeue())
    print("removing ",queue.dequeue())

    print("removing ",queue.dequeue())
    print("removing ",queue.dequeue())

