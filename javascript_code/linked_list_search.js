class Node {
    constructor(data) {
        this.data = data
        this.next = null
    }
}

class LinkedList {
    constructor() {
        this.head = null
    }

    insert(data) {
        let new_node = new Node(data); 
        if(data == null || data == undefined) {
            console.log("please pass the data")
            return;
        } 
        
        if(!this.head) {
            this.head = new_node
            return;
        }

        let cur_node = this.head
        while(cur_node.next) {
            cur_node = cur_node.next
        }
        cur_node.next = new_node;

    }

    search(key) {
        let pos = 1;
        if(key == null || key == undefined) {
            console.log("please pass the key")
            return;
        }

        if(!this.head) {
            console.log("list is  empty");
            return;
        }

        let cur_node = this.head;
        while(cur_node) {
            if(cur_node.data == key) {
                console.log("element found at position ",pos);
                return;
            }
            cur_node = cur_node.next;
            pos = pos+1;
        }
        console.log("element not found");

    }
}

let llist = new LinkedList();
llist.insert(1);
llist.insert(2);
llist.insert(3);
llist.insert(4);
llist.search(4)