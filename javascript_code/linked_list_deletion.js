/**
 * @class Node
 * @param data
 * @returns none
 * @author saikrishna
 */
class Node {
    constructor(data) {
        this.data = data;
        this.next = null;
    }
}

class LinkedList {
    constructor(data) {
        this.head = null; 
    }

    insertFront(data) {
        let new_node = new Node(data);
        if(this.head == null) {
            this.head = new_node
        }else {
            let cur_node = this.head;
            while(cur_node.next) {
                cur_node = cur_node.next;
            }
            cur_node.next = new_node;
            console.log("inserting")
        }
    }

    deleteNode(key) {
        let pos = 1;
        let cur_node = this.head;
        let prev_node = null;
        if(cur_node == null) {
            console.log("nothing to delete");
        }
        else if(cur_node.data == key) {
            this.head = cur_node.next;
        }else {
            while(cur_node.data != key) {
                prev_node = cur_node;
                cur_node = cur_node.next;
                pos =pos + 1;
            }
            prev_node.next = cur_node.next;
            console.log("deleting node at position",pos)
        }
    }


    printList() {
        let cur_node = this.head;
        while(cur_node.next) {
            process.stdout.write(cur_node.data + " --> ");
            cur_node = cur_node.next;
        }
        process.stdout.write(cur_node.data + " --> ")
        process.stdout.write("None\n");
    }
}


llist = new LinkedList();
llist.insertFront(1);
llist.insertFront(2);
llist.insertFront(3);
llist.insertFront(4);
llist.printList();
llist.deleteNode(1);
llist.printList();