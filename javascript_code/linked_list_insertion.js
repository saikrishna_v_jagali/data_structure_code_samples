class Node {
    constructor(data){
        this.data = data;
        this.next = null;
    }
}


class LinkedList {
    constructor() {
        this.head = null;
    }

    insertFront(data) {
        if(this.head == null) {
            console.log("inserting new **HEAD** [",data,"] from the front")
            let new_node = new Node(data);
            this.head = new_node;
            this.count++;
        }

        else {
            console.log("inserting node [",data,"] from the front")
            let new_node = new Node(data);
            new_node.next = this.head ;
            this.head = new_node;
            this.count++;
        }
    }

    insertRear(data) {
        if(this.head == null) {
            console.log("inserting new **HEAD** [",data,"] from the rear");
            let new_node = new Node(data);
            this.head = new_node;
            this.count++;

        }else{
            console.log("inserting new node [",data,"] from the rear");
            let new_node = new Node(data);
            let next_node = this.head;
            while(next_node.next) {
                next_node = next_node.next;
            }
            next_node.next = new_node;
            this.count++;
        }
    }

    insertAtPosition(data,insert_pos) {
        let new_node = new Node(data);
        let cur_node = this.head;
        let pos = 1;
        if(cur_node == null && insert_pos == 1) {
            console.log("inserting the node as the head node of linkedlist");
            this.head = new_node;
            this.count = this.count + 1;
        }
        else if(insert_pos >  this.count) {
            console.log("position is higher than greater than list length");
        } 
        else {
            while(pos < insert_pos-1) {
                pos = pos + 1;
                cur_node = cur_node.next;
            }
            console.log("inserting node at position ",pos)
            new_node.next = cur_node.next;
            cur_node.next = new_node;
            this.count += 1;
        }
    }

    printList() {
        let temp_node  = this.head;
        while(temp_node) {
            process.stdout.write(`${temp_node.data} --> `);
            temp_node = temp_node.next
        }
        console.log(temp_node);
    }
}

// const llist = new LinkedList
// llist.insertFront(1);
// llist.insertFront(2);
// llist.insertFront(3);
// llist.printList();

// const llist_rear = new LinkedList();
// llist_rear.insertRear(1);
// llist_rear.insertRear(2);
// llist_rear.insertRear(3);
// llist_rear.printList();


const llist_insertAtPos = new LinkedList()
llist_insertAtPos.insertAtPosition(1,1)
llist_insertAtPos.insertAtPosition(2,2)
llist_insertAtPos.insertAtPosition(3,2)
llist_insertAtPos.insertAtPosition(4,2)
llist_insertAtPos.printList()
