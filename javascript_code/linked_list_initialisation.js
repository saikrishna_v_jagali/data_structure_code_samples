
// initialise the linkedlist class
class Node {
    constructor(data) {
        this.data = data;
        this.next = null;
    }
}

class  LinkedList {
    constructor() {
        this.head = null;

    }
}
//create the first one
const llist = new LinkedList();
llist.head =  new Node(12);
llist.head.next = new Node(33);
llist.head.next.next = new Node(44);

console.log(llist.head.data);
console.log(llist.head.next.data);
console.log(llist.head.next.next.data);

